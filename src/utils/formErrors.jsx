import React, {Component} from 'react';
import _ from "lodash";

export const ERROR_MESSAGES = {
    TROP_COURT : "Trop court",
    EMAIL_INVALID : "Email invalide",
    PDF_WORD_DEMANDE : "Merci de charger un fichier pdf ou word",
    DOCUMENT_INF_500KB : "Le document doit être inférieur à 500kB",
    DOCUMENT_INF_2MB : "Le document doit être inférieur à 2MB"
}

export const FormErrors = ({formErrors, inputRef, errorMessage}) =>
    <div className='formErrors'>
        { (Object.keys(_.pickBy(formErrors, _.identity))).map( field => {
            if(field === inputRef) {return <label className="messageErreur">{errorMessage}</label> ; }
            else { return '' ;}
            })
        }
    </div>


