import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, BrowserRouter, Router, Route} from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';


import {Provider} from "react-redux";
import {createStore, applyMiddleware, compose} from "redux";
import reducers from "./reducers/reducers";
import ReduxPromise from "redux-promise";
import Main from "./containers/Main_container";
import history from "./js/history";
import Thunk from "redux-thunk";




// Creation du store
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(
    applyMiddleware(ReduxPromise, Thunk),
    // other store enhancers if any
);
const store = createStore(reducers, enhancer);


export default ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route component={Main}/>
        </Router>
    </Provider>,

    document.getElementById('root'));
registerServiceWorker();




