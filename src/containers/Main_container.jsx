import React, {Component} from 'react';
import {ToastContainer} from "react-toastify";

import {Route} from "react-router-dom";
import {PATH} from "../routes";
import Home from "../components/home"


export default class Main extends Component {

    constructor(props) {
        super(props);
    }


    render() {

        return (
            <div id="MainContainer">

                <div id="Header">
                    <h1>MENU</h1>
                    <ToastContainer/>
                </div>
                <div id="Content">
                    <Route exact path={PATH.HOME} component={Home}/>
                </div>
            </div>
        )

    }
}


